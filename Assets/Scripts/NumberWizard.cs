﻿//using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NumberWizard : MonoBehaviour
{
    [SerializeField] int min;
    [SerializeField] int max;
    [SerializeField] TextMeshProUGUI guessText;
    int guess;

    // Start is called before the first frame update
    void Start()
    {
        StartGame();
    }

    private void StartGame() {
        max++;
        NextGuess();
    }
    
    public void OnPressHigher() {
        if (guess < max) {
            min = guess + 1;
            NextGuess();
        }
    }

    public void OnPressLower() {
        if (guess > min) {
            max = guess;
            NextGuess();
        }
    }

    void NextGuess() {
        guess = Random.Range(min, max);
        guessText.text = guess.ToString();
    }
}
